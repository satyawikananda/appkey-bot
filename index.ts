import { bot } from './helpers/bot';
import * as dotenv from 'dotenv';
import checkIn from './commands/checkIn';
import checkOut from './commands/checkOut';
import restIn from './commands/restIn';
import restOut from './commands/restOut';
import author from './commands/author';

dotenv.config();
bot.use((_, next) => {
  next();
});

checkIn(bot);
checkOut(bot);
restIn(bot);
restOut(bot);
author(bot);

// launch bot here
bot
  .launch()
  .then(() => {
    console.log('Bot is running');
  })
  .catch((err) => {
    console.error(err);
  });
