import { Telegraf, Context } from 'telegraf';
import { rowRestOut } from '../helpers/spreadsheet';
import moment from 'moment-timezone';
import * as dotenv from 'dotenv';

dotenv.config();
moment.locale('id');

export default function restOut(bot: Telegraf<Context>) {
  bot.command('breakout', async (ctx) => {
    const getChat = await ctx.getChatMember(ctx.from.id);
    await rowRestOut({
      Nama: getChat.user.first_name,
      IstirahatKeluar: moment().tz('Asia/Makassar').format('h:mm:ss a'),
      Tanggal: moment().tz('Asia/Makassar').format('dddd, MMMM Do YYYY'),
    });
    return ctx.reply(
      `Terimakasih sudah melakukan absen istirahat keluar disini ${
        '@' + getChat.user.username
      }. Makan yang banyak ya 🤙😎!!`
    );
  });
}
