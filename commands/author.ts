import { Telegraf, Context } from 'telegraf';
import * as dotenv from 'dotenv';

dotenv.config();

export default function author(bot: Telegraf<Context>) {
  bot.command('author', async (ctx) => {
    let text: string = `Bot ini dibuat oleh ${'@satyawikananda'} menggunakan \`Typescript\` dan \`Telegraf\`\n`;
    text +=
      'Yang punya github bisa difollow github author ya: https://github.com/satyawikananda';
    return ctx.replyWithMarkdown(text);
  });
}
