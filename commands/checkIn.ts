import { Telegraf, Context } from 'telegraf';
import { rowCheckIn } from '../helpers/spreadsheet';
import moment from 'moment-timezone';
import * as dotenv from 'dotenv';

dotenv.config();
moment.locale('id');

export default function checkIn(bot: Telegraf<Context>) {
  bot.command('start', async (ctx) => {
    const getChat = await ctx.getChatMember(ctx.from.id);
    await rowCheckIn({
      Nama: getChat.user.first_name,
      AbsenHadir: moment().tz('Asia/Makassar').format('h:mm:ss a'),
      Tanggal: moment().tz('Asia/Makassar').format('dddd, MMMM Do YYYY'),
    });
    console.log(getChat.user.username);
    return ctx.reply(
      `Terimakasih sudah melakukan absen hadir disini ${
        '@' + getChat.user.username
      }, semangat ya!!`
    );
  });
}
