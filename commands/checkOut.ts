import { Telegraf, Context } from 'telegraf';
import { rowCheckOut } from '../helpers/spreadsheet';
import moment from 'moment-timezone';
import * as dotenv from 'dotenv';

dotenv.config();

moment.locale('id');

export default function checkOut(bot: Telegraf<Context>) {
  bot.command('end', async (ctx) => {
    const getChat = await ctx.getChatMember(ctx.from.id);
    await rowCheckOut({
      Nama: getChat.user.first_name,
      AbsenKeluar: moment().tz('Asia/Makassar').format('h:mm:ss a'),
      Tanggal: moment().tz('Asia/Makassar').format('dddd, MMMM Do YYYY'),
    });
    return ctx.reply(
      `Terimakasih sudah melakukan absen keluar disini ${
        '@' + getChat.user.username
      },　お疲れ様でした　😊!!`
    );
  });
}
