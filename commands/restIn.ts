import { Telegraf, Context } from 'telegraf';
import { rowRestIn } from '../helpers/spreadsheet';
import moment from 'moment-timezone';
import * as dotenv from 'dotenv';

dotenv.config();

moment.locale('id');

export default function restIn(bot: Telegraf<Context>) {
  bot.command('breakin', async (ctx) => {
    const getChat = await ctx.getChatMember(ctx.from.id);
    await rowRestIn({
      Nama: getChat.user.first_name,
      IstirahatMasuk: moment().tz('Asia/Makassar').format('h:mm:ss a'),
      Tanggal: moment().tz('Asia/Makassar').format('dddd, MMMM Do YYYY'),
    });
    return ctx.reply(
      `Terimakasih sudah melakukan absen istirahat masuk disini ${
        '@' + getChat.user.username
      }, semangat lagi ya kerjanya 😊!!`
    );
  });
}
