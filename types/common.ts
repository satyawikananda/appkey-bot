export interface DataRowCheckIn {
  Nama?: string | number | boolean;
  AbsenHadir?: string | number | boolean;
  Tanggal?: string | number | boolean;
}

export interface DataRowCheckOut {
  Nama?: string | number | boolean;
  AbsenKeluar?: string | number | boolean;
  Tanggal?: string | number | boolean;
}

export interface DataRowIstirahatMasuk {
  Nama?: string | number | boolean;
  IstirahatMasuk?: string | number | boolean;
  Tanggal?: string | number | boolean;
}

export interface DataRowIstirahatKeluar {
  Nama?: string | number | boolean;
  IstirahatKeluar?: string | number | boolean;
  Tanggal?: string | number | boolean;
}
