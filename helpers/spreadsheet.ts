import {
  GoogleSpreadsheet,
  ServiceAccountCredentials,
} from 'google-spreadsheet';
import * as dotenv from 'dotenv';
import {
  DataRowCheckIn,
  DataRowCheckOut,
  DataRowIstirahatKeluar,
  DataRowIstirahatMasuk,
} from '../types/common';

dotenv.config();

const SPREADSHEET_ID: string = process.env.SPREADSHEET_ID;
const doc = new GoogleSpreadsheet(SPREADSHEET_ID);
const creedOptions: ServiceAccountCredentials = {
  client_email: process.env.GOOGLE_SERVICE_ACCOUNT_EMAIL,
  private_key: process.env.GOOGLE_PRIVATE_KEY.replace(/\\n/g, '\n'),
};

export async function rowCheckIn({
  Nama,
  AbsenHadir,
  Tanggal,
}: DataRowCheckIn): Promise<any> {
  await doc.useServiceAccountAuth(creedOptions);
  await doc.loadInfo();
  const sheet = doc.sheetsByIndex[0];
  const addRow = await sheet.addRow({
    Nama: Nama ?? '-',
    'Absen hadir': AbsenHadir ?? '-',
    Tanggal: Tanggal ?? '-',
  });
  const saveRow = await addRow.save();

  return Promise.resolve(saveRow);
}

export async function rowCheckOut({
  Nama,
  AbsenKeluar,
  Tanggal,
}: DataRowCheckOut): Promise<any> {
  await doc.useServiceAccountAuth(creedOptions);
  await doc.loadInfo();
  const sheet = doc.sheetsByIndex[3];
  const addRow = await sheet.addRow({
    Nama: Nama ?? '-',
    'Absen keluar': AbsenKeluar ?? '',
    Tanggal: Tanggal ?? '-',
  });
  const saveRow = await addRow.save();
  return Promise.resolve(saveRow);
}

export async function rowRestIn({
  Nama,
  IstirahatMasuk,
  Tanggal,
}: DataRowIstirahatMasuk): Promise<any> {
  await doc.useServiceAccountAuth(creedOptions);
  await doc.loadInfo();
  const sheet = doc.sheetsByIndex[2];
  const addRow = await sheet.addRow({
    Nama: Nama ?? '-',
    'Istirahat masuk': IstirahatMasuk ?? '',
    Tanggal: Tanggal ?? '-',
  });
  const saveRow = await addRow.save();
  return Promise.resolve(saveRow);
}

export async function rowRestOut({
  Nama,
  IstirahatKeluar,
  Tanggal,
}: DataRowIstirahatKeluar): Promise<any> {
  await doc.useServiceAccountAuth(creedOptions);
  await doc.loadInfo();
  const sheet = doc.sheetsByIndex[1];
  const addRow = await sheet.addRow({
    Nama: Nama ?? '-',
    'Istirahat keluar': IstirahatKeluar ?? '',
    Tanggal: Tanggal ?? '-',
  });
  const saveRow = await addRow.save();
  return Promise.resolve(saveRow);
}
