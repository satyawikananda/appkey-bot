import cron from 'cron';
const InitCron = cron.CronJob;

export default function schedule(cron: string, cb: any): void {
  const job = new InitCron(cron, cb, null, true, 'Asia/Makassar');
  job.start();
}
